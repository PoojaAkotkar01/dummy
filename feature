Feature: hotel room booking

  @execute
  Scenario: User will try to book hotel room with invalid login credentials
     Given User is on hotel room booking page
     When User clicks 'Login' button after entering invalid 'UserName' and 'Password'
     Then 'valid login! Please try again!' message should display
  
   @execute
  Scenario: User will try to book hotel room with valid login credentials
     Given User is on hotel room booking page
     When User clicks 'Login' button after entering valid 'UserName' and 'Password'
     Then User is navigated to next page
     
    @execute
  Scenario: User will try to book hotel room with valid set of information
  	Given User is on hotel room booking page
  	When User clicks 'Confirm Booking' button without entering 'FirstName'
    Then 'Please fill the First Name' message should display
    When User clicks 'Confirm Booking' button without entering 'LastName'
    Then 'Please fill the Last Name' message should display
    When User clicks 'Confirm Booking' button without entering 'Email'
    Then 'Please fill the Email' message should display
    When User clicks 'Confirm Booking' button after entering invalid 'Email' 
    Then 'Please enter valid Email Id.' message should display
    When User clicks 'Confirm Booking' button without entering 'Mobile No'
    Then 'Please fill the Mobile No.' message should display
    When User clicks 'Confirm Booking' button  after entering invalid 'Mobile No'
    Then 'Please enter valid Contact no.' message should display
   	When User clicks 'Confirm Booking' button without selecting 'City'
    Then 'Please select city' message should display
    When User clicks 'Confirm Booking' button without selecting 'State'
    Then 'Please select state' message should display
    When User clicks 'Confirm Booking' button without entering 'Card Holder Name'
    Then 'Please fill the Card holder name' message should display
    When User clicks 'Confirm Booking' button without entering 'Debit Card Number'
    Then 'Please fill the Debit card Number' message should display
    When User clicks 'Confirm Booking' button without entering 'CVV'
    Then 'Please fill the CVV' message should display
    When User clicks 'Confirm Booking' button without entering 'Expiration Month'
    Then 'Please fill expiration month' message should display
    When User clicks 'Confirm Booking' button without entering 'Expiration Year'
    Then 'Please fill the expiration year' message should display
    
     @execute
  Scenario: User will try to book hotel room with valid set of information
  	Given User is on hotel room booking page
  	When User clicks 'Confirm Booking' button after entering Valid set of information
    Then 'Booking Completed!' message should display